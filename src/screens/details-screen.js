import React from 'react';
import { Text, View, Button } from 'react-native';

export class DetailsScreen extends React.Component {
    static navigationOptions = {
        title: 'Details',
    };

    render() {
        return (
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                <Text>Details Screen</Text>
                <Button
                    title="Nochmal zu den Details!"
                    // Wir sind schon auf der Detail Seite. Navigate hätte keinen Effekt,
                    // deshalb wird mit push() das navigieren erzwungen
                    onPress={() => this.props.navigation.push('Details')}
                />
                <Button
                    title="Zur Startseite"
                    onPress={() => this.props.navigation.navigate('Home')}
                />
                <Button title="Zurück" onPress={() => this.props.navigation.goBack()} />
            </View>
        );
    }
}
