import React from 'react';
import { Text, View, Image, TextInput, Button, StyleSheet, Alert } from 'react-native';
import { Hello } from './../components/hello-component';
import { Blink } from './../components/blink-component';

export class HomeScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = { text: '' };
    }

    static navigationOptions = {
        title: 'Willkommen',
    };

    render() {
        let pic = {
            uri:
                'https://upload.wikimedia.org/wikipedia/commons/thumb/c/c7/Hochschule_f%C3%BCr_Telekommunikation_Leipzig_Logo.svg/1000px-Hochschule_f%C3%BCr_Telekommunikation_Leipzig_Logo.svg.png',
        };
        return (
            // <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            //     <Text>Home Screen</Text>
            // </View>
            <View style={styles.container}>
                <Image source={pic} style={{ width: 250, height: 100 }} />
                <Hello style={[{ color: '#736fff' }, styles.italic]} name="Welt" />
                <Hello name="DWI17" />
                <Blink style={{ color: 'magenta' }} text="Hier blinkt's magenta und fett!" />
                <Blink text="Hier blinkt's fett!" />
                <TextInput
                    placeholder="Hier tippen!"
                    onChangeText={text => this.setState({ text })}
                />
                <Text style={{ fontSize: 30 }}>
                    {this.state.text === '' ? 'nichts eingegeben' : this.state.text.toUpperCase()}
                </Text>
                <Button title="Alert!" onPress={this.buttonPressed} />
                <Button
                    title="Zu den Details!"
                    onPress={() => this.props.navigation.navigate('Details')}
                />
            </View>
        );
    }

    buttonPressed() {
        Alert.alert('Der Button wurde Angetippt!');
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        // paddingTop: 30,
    },
    italic: {
        fontStyle: 'italic',
    },
});
