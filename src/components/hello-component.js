import React from 'react';
import { Text, View } from 'react-native';

export class Hello extends React.Component {
    render() {
        return (
            <View>
                <Text style={this.props.style}>Hallo {this.props.name}</Text>
            </View>
        );
    }
}
