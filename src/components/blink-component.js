import React from 'react';
import { Text, StyleSheet } from 'react-native';

export class Blink extends React.Component {
    constructor(props) {
        super(props);
        this.state = { isShowingText: true };

        setInterval(() => {
            // Die setState Funktion alle 1000 ms aufrufen
            // Der Aufruf sorgt für das neu rendern der Komponente
            this.setState(previousState => {
                // Den Wert, der isShowingText vorher hatte negieren und zurückgeben
                return { isShowingText: !previousState.isShowingText };
            });
        }, 1000);
    }
    render() {
        if (this.state.isShowingText) {
            return <Text style={[this.props.style, styles.bold]}>{this.props.text}</Text>;
        }

        return <Text>&nbsp;</Text>;
    }
}

const styles = StyleSheet.create({
    bold: {
        fontWeight: 'bold',
    },
});
