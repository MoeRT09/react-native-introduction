import { createStackNavigator, createAppContainer } from 'react-navigation';
import { HomeScreen } from './screens/home-screen';
import { DetailsScreen } from './screens/details-screen';

const AppNavigator = createStackNavigator(
    {
        Home: HomeScreen,
        Details: DetailsScreen,
    },
    {
        initialRouteName: 'Home',
    }
);

export const AppContainer = createAppContainer(AppNavigator);
