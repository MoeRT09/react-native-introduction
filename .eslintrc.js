module.exports = {
    "env": {
        "es6": true,
        "node": true
    },
    "extends": [
        "expo/native"
    ],
    "globals": {
        "Atomics": "readonly",
        "SharedArrayBuffer": "readonly"
    },
    "parserOptions": {
        "ecmaFeatures": {
            "jsx": true
        },
        "ecmaVersion": 2018
    },
    "plugins": [
    ],
    "rules": {
    }
};