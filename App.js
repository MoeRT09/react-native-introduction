import React from 'react';
import { AppContainer } from './src/app-container';

export default class App extends React.Component {
    render() {
        return <AppContainer />;
    }
}
