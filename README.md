# React Native Einführung

## Vorbereitung

1. Installation von [Node.JS](https://nodejs.org/en/). LTS-Version wird empfohlen!
    - sicherstellen, dass NPM installiert wird und die Option "add to PATH" aktiviert ist
2. Installation verifizieren:
    - Eingabeaufforderung öffnen (`Win + R` -> `cmd`)
    - `node -v` sollte die installierte Node.JS Version zeigen, z.B. `v11.11.0`
    - `npm -v` sollte die Version des Node Package Managers (NPM) zeigen, z.B. `6.7.0`
3. Installation der Expo CLI: `npm install expo-cli --global`
    - nutzt NPM zum download des Paketes und der benötigten Abhängigkeiten und installiert dieses als globale Anwendung
    - stellt den `expo` Befehl zur Verfügung
4. Ein Git client muss installiert sein: [Download](https://git-scm.com/downloads). Die Einrichtung wie [hier](https://de.atlassian.com/git/tutorials/install-git#windows) gezeigt durchführen.
5. Einen Editor zum bearbeiten der Quelltexte installieren (zur Not geht auch das Windows Notepad)
    - z.B. [Visual Studio Code](https://code.visualstudio.com/) mit Erweiterung [React Native Tools](https://marketplace.visualstudio.com/items?itemName=vsmobile.vscode-react-native)
    - Empfehlung: Visual Studio Code kann den Quellcode automatisch auf Fehler prüfen und zeigt diese unterstrichen an. Hierfür muss die Erweiterung [ESLint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint) installiert werden.
    - Zur Vereinheitlichung der Code-Formatierung, die Erweiterung [Prettier](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode). Code kann durch drücken von `F1` -> `Format Document` automatisch nach definierten Regeln formatiert werden.
    - fehlende imports automatisch hinzufügen mittels Erweiterung [Auto Import - ES6, TS, JSX, TSX](https://marketplace.visualstudio.com/items?itemName=NuclleaR.vscode-extension-auto-import)
5. (Optional) Android Studio installieren (Notwendig für den Emulator)
    - [Download](https://developer.android.com/studio)
    - sicherstellen, dass "Android Virtual Device" aktiviert ist
6. Emulator einrichten
    - Android Studio starten
    - Im Setup Wizard am Punkt "SDK Components Setup" Einstellungen so belassen, aber "Android Virtual Device" ankreuzen
7. Emulator Starten
    - Im Bildschirm "Welcome to Android Studio" unten auf "Configure" klicken, dann auf "AVD Manager"
    - es sollte bereits ein Gerät vorhanden sein, dieses kann über den grünen Pfeil gestartet werden
    - sobald das Gerät gestartet ist, kann es die Anwendung ausführen

## Neues Projekt anlegen
**Diese Schritte sind nur einmalig durchzuführen. Für bestehende Projekte, siehe Abschnitt "Projekt von GitLab clonen"**
1. In einem Terminal: `expo init <Projektname>`
2. Bei der Frage "Choose a template" "blank" wählen
3. Einen Namen vergeben, wie er später im App drawer angezeigt werden soll. Expo lädt die benötigten Abhängigkeiten herunter und erstellt das Projektverzeichnis.
4. (Optional aber empfohlen) Quellcode Linting für das Projekt einrichten (automatisches Prüfen auf Syntaxfehler):
    - ESLint Prettier (für einheitliche Code-Formatierung) und react native linter zum Projekt hinzufügen (im Projektverzeichnis):  
    `npm install --save-dev eslint prettier eslint-config-expo`
    - Die Datei `.eslintrc.js` im Projektverzeichnis erstellen mit folgendem Inhalt:  
    ```javascript
    module.exports = {
        "env": {
            "es6": true,
            "node": true
        },
        "extends": [
            "expo/native"
        ],
        "globals": {
            "Atomics": "readonly",
            "SharedArrayBuffer": "readonly"
        },
        "parserOptions": {
            "ecmaFeatures": {
                "jsx": true
            },
            "ecmaVersion": 2018
        },
        "plugins": [
        ],
        "rules": {
        }
    };
    ```
    - Die Datei `.prettierrc` im Projektverzeichnis erstellen, mit folgendem Inhalt:
    ```json
    {
        "printWidth": 100,
        "tabWidth": 4,
        "singleQuote": true,
        "jsxBracketSameLine": true,
        "trailingComma": "es5",
        "useTabs": false
    }
    ```

## Projekt zu GitLab hinzufügen
**Die folgenden Schritte sind nur einmalig durchzuführen umd das Projekt anzulegen und zu initialisieren. Um ein bestehendes Projekt von GitLab auf den eigenen Rechner zu holen, siehe nächster Abschnitt!**
1. Die Git Bash starten und ins Projektverzeichnis navigieren (`cd <Pfad zum Projektverzeichnis>` z.B. `cd /c/Users/administrator/Documents/react-project` **Groß- und Kleinschreibung beachten und `/` statt `\` verwenden**)  
**ODER**  
Mit dem Windows Explorer ins Projektverzeichnis navigieren -> rechtsklick -> "Open Git Bash Here"
2. Die React Native `.gitignore` Datei [herunterladen](https://raw.githubusercontent.com/expo/expo/master/.gitignore) und in der untersten Ebene des Projektverzeichnisses speichern (der Punkt am Anfang ist wichtig!). Diese verhindert, das temporäre Dateien bzw. Dateien, die nur lokal relevant sind mit unter Versionskontrolle gestellt werden. Insbesondere sollte das Verzeichnis `node_modules` **nicht** versioniert werden!
3. Ein neues, leeres Projekt auf [GitLab](https://gitlab.com) anlegen
4. auf "Clone" klicken und die clone URL kopieren (SSH oder HTTPS)
5. Mit der Git Bash im Projektverzeichnis:
    ```bash
    git remote add origin <clone URL>        # GitLab als remote repository definieren
    git add .                                # Die Dateien des Projektverzeichnisses zum commit vormerken
    git commit -m "initial commit"           # Die Änderungen einchecken
    git push --set-upstream origin master    # Änderungen in das remote repository laden
    ```

## Projekt von GitLab klonen
1. Zunächst, wird die clone URL benötigt. Dazu die Projektseite auf [GitLab](https://gitlab.com) öffnen, auf den Button "Clone" klicken und eine der URLs kopieren.
2. Die Git Bash starten und in ein Verzeichnis navigieren, in dem das Projekt als Unterordner angelegt werden soll (`cd <Pfad zum Verzeichnis>` z.B. `cd /c/Users/administrator/Documents` **Groß- und Kleinschreibung beachten und `/` statt `\` verwenden**)  
**ODER**  
Mit dem Windows Explorer ins Verzeichnis navigieren -> rechtsklick -> "Open Git Bash Here"
3. In der Git Bash: `git clone <clone URL>` um das Projekt von GitLab zu Klonen
4. mit `cd <Projektname>` ins Projektverzeichnis wechseln
5. mit `npm install` die Projektabhängigkeiten installieren (Ordner `node_modules` wird angelegt)
6. Das Projekt kann nun im Abschnitt "Das Projekt Ausführen" beschrieben gestartet werden

## Das Projekt Ausführen
1. mit `cd <Projektname>` in das Projektverzeichnis wechseln
2. mit `npm start` wird das Projekt kompiliert und ein Entwicklungsserver gestartet

### Projekt auf echtem Gerät starten
Um das Projekt auf einem Physischen Android Gerät zu starten, muss dazu:
1. Die [Expo App](https://play.google.com/store/apps/details?id=host.exp.exponent&hl=de) auf dem Gerät installiert werden.
2. Die Expo App gestartet, und der QR-Code, der im Terminal oder im Browser angezeigt wird gescannt werden.  
**Wichtig: das Smartphone muss sich im gleichen Netz (WLAN/LAN) befinden, wie der Entwicklungsrechner!**

### Projekt im Emulator starten
1. Der Emulator muss gestartet sein (siehe Punkt 6 der Vorbereitung)
2. Durch drücken von `a` im Terminal bzw. "Run on Android device/emulator", wird die App im Emulator installiert und gestartet

## Das Projekt bearbeiten
### Dokumentationen
- [Expo](https://docs.expo.io/versions/v32.0.0/)
- [React Native](https://devdocs.io/react_native/)

